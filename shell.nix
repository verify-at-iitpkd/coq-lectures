with import <nixpkgs> {};
stdenv.mkDerivation {
  name = "env";
  buildInputs = [
    emacs
    gnumake
    jre
    ditaa
    emacsPackages.htmlize
  ];
  shellHook = ''
  export DITAA_JAR="${ditaa.outPath}/lib/ditaa.jar"
  '';
}
