(*

Some stuff here.

*)

(* * TYPES *)
Check 42.
Check true.
Check 42 + 2.
Check (42,true).
Check fun x : nat => x + 1.
Check fun x : nat => true.
(* END_TYPES *)

(* * BOOLEAN *)


Inductive boolean  :=
| bTrue  : boolean
| bFalse : boolean.

Check bTrue.
Check bFalse.

(* Exercise: Define an inductive type that captures the days of the week. *)
(* END_BOOLEAN *)


(* * BOOLEAN_FUNCTIONS *)

Definition bNot (b : boolean) : boolean :=

  match b with (* Pattern matching on b *)
  | bTrue => bFalse
  | bFalse => bTrue
  end.

(*

Definition name_of_the_function argdec* [: term] := term.

argdec : ident : term      ident is an identifier and term is a Gallina term (which should be a type)

 *)

Definition bAnd (a b : boolean) : boolean :=
  match a with
  | bTrue => b
  | _     => bFalse
  end.

Compute bNot bTrue.
Compute bNot bFalse.
Compute bAnd bFalse bFalse.
Check bAnd bFalse bFalse.

(* Exercise: Write the function bOr which takes the "or" of two
   boolean values.

    Exercise 2 : imagine a week with 5 working days (Monday --
    Friday), write the function nextWorkingDay : DayOfWeek ->
    DayOfWeek. It should give the next working day.


*)
(* * END_BOOLEAN_FUNCTIONS *)


(* * NATS *)

Inductive Nat :=
| Zero : Nat          (* Zero is a natural number  *)
| Succ : Nat -> Nat.  (* n : Nat then Succ n : Nat *)


Print nat.
Print Nat.

(* * END_NATS *)

(* * NAT_ADDITION *)

Definition isZero (a : Nat) : boolean :=
  match a with
  | Zero     => bTrue
  | Succ _   => bFalse
  end.

Fixpoint plus (a b : Nat) : Nat :=
  match a with
  | Zero     => b
  | Succ ap  => Succ (plus ap b)
  end.

(* Definition of plus not using the Fixpoint Vernacular *)

Definition pls := fix p (a b : Nat) : Nat :=
                    match a with
                    | Succ ap => Succ (p ap b)
                    | Zero    => b
                    end.
(*

Take the function F p a b => match a with
                              | Succ ap => Succ (p ap b)
                              | Zero     => b
                             end.

*)


Definition two := Succ (Succ Zero).

Compute plus two two.

(* Exercise : Write a function to do multiplication of naturals.

mul : Nat -> Nat -> Nat.

 *)

(* * END_NAT_ADDITION *)

(* * NAT_THEOREMS *)

Theorem left_identity : forall n : Nat, plus Zero n = n. (* This is one way to state it *)
Proof.
  (* Proof steps will go here *)
  trivial.
Qed.

Theorem left_identity_alternate (n : Nat) : plus Zero n = n.
  trivial.
  Show Proof.
Qed.

Definition left_id : forall n : Nat, plus Zero n = n := fun n => eq_refl.
Print left_identity.
Print left_id.

(* Theorem left_identity : ....

   Definition left_identity : ... := ....


*)


Theorem right_identity : forall n : Nat, plus n Zero = n.
  (* Unlike in the previous case trivial will not work *)
  intro n.
  induction n.
  - trivial.  (* Base case *)
  - simpl. rewrite IHn. trivial. (* induction step *)

    Show Proof.

Qed.

Require Import Arith.
Search (_ + 0 = _).
Lemma apbsquare : forall a b : nat,  (a + b)^2 = a^2 + b^2 + 2 * a * b.
  intros.
  simpl.
  ring.
  Show Proof.
Qed.
(* END_NAT_THEOREMS *)
