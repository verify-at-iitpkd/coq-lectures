(*  THEOREM_VS_DEF    *)
Definition foo : nat := 2.

Theorem two_is_two : 2 = 2.
  Show Proof.
  trivial.
  Show Proof.
Qed.

Definition bar (n : nat) : nat :=
  match n with
  | _ => 0
  end.

Theorem barp : nat -> nat.
  refine (fun n => match n with
                   | 0 => 0
                   | _ => 0
                   end).
Defined.

Theorem barpp : nat -> nat.
  refine (fun n => match n with
                   | 0 => 0
                   | _ => 0
                   end).
Qed.

Compute (barp 2, barpp 2).

Definition two_is_two_p : 2 = 2 := eq_refl.
Theorem foo_p : nat.
  exact 2.
Qed.
(* END_THEOREM_VS_DEF *)

(* PREDICATE *)
Definition left_identity n := 0 + n = n.
Check left_identity.

Definition proof_of_left_id : forall n , left_identity n :=
  fun n => eq_refl.

Check proof_of_left_id.
(* END_PREDICATE *)


(* EXAMPLE *)

Definition T (n : nat) : Type :=
  match n with
  | 0 => nat
  | 1 => bool
  | _ => (nat * bool)
  end.

Check T.
Compute T 42.

Definition all_n_T  : forall n , T n :=
  fun n => match n with
           | 0 => 42
           | 1 => false
           | m => (m , true)
           end.
Check all_n_T.
Compute all_n_T 4.
(* END_EXAMPLE *)


(* LIST_HEAD *)

Require Import List.
Import ListNotations.

Compute [].
Compute [1].
Compute [1;2].
Compute 1 :: [2].

Print list.

(*
 NonEmpty l : Should assert that l is non-empty.
*)
Inductive NonEmpty {A} : list A -> Prop
  := nonEmpty : forall (a : A) (l : list A), NonEmpty (a :: l).

(*
nonEmpty constructor says that I can create a proof of NonEmpty (a :: l) given a and l

*)


Lemma nil_is_empty A : ~ (@NonEmpty A []).
  compute.
  intro Hyp.
  inversion Hyp.
Qed.

Print list.

(*
Inductive list (A : Type) : Type :=
| nil : list A
| cons : A -> list A -> list A.

*)

Definition head {A : Type} (l : list A) : NonEmpty l -> A.
  refine (match l with
          | cons a _  => fun _ => a
          | nil       => _
          end).
  intro Hyp.
  assert (fHyp:False) by exact (nil_is_empty A Hyp).
  destruct fHyp.
  Show Proof.
Defined.

Program Definition hd {A : Type} (l : list A) : NonEmpty l -> A
  := match l with
     | cons a _  => fun _ => a
     | nil     => _
     end.

Next Obligation.
  assert (fHyp:False).
  exact (nil_is_empty A H).
  destruct fHyp.
Qed.

Print hd.



Hint Resolve nil_is_empty.

Ltac crush := repeat match goal with
                     | [ |- nat -> _ ] => let n := fresh "n" in intro n; induction n
                     | _ => repeat simpl; eauto
                     end.

Lemma right_id : forall n, n + 0 = n.
  crush.
Qed.

Hint Resolve right_id.
Lemma commutative : forall m n, m + n = n + m.
  crush.
  rewrite <- IHn0.
  crush.
Qed.
