(*  THEOREM_VS_DEF    *)
Definition foo : nat := 2.
Theorem two_is_two : 2 = 2.
  trivial.
  Show Proof.
Qed.

Definition two_is_two_p : 2 = 2 := eq_refl.
Theorem foo_p : nat.
  exact 2.
Qed.
(* END_THEOREM_VS_DEF *)

(* PREDICATE *)
Definition left_identity n := 0 + n = n.
Check left_identity.

Definition proof_of_left_id : forall n , left_identity n :=
  fun n => eq_refl.

Check proof_of_left_id.
(* END_PREDICATE *)


(* EXAMPLE *)

Definition T (n : nat) : Type :=
  match n with
  | 0 => nat
  | 1 => bool
  | _ => (nat * bool)
  end.

Check T.
Compute T 42.

Definition all_n_T  : forall n , T n :=
  fun n => match n with
           | 0 => 42
           | 1 => false
           | m => (m , true)
           end.
Check all_n_T.
Compute all_n_T 4.
(* END_EXAMPLE *)
