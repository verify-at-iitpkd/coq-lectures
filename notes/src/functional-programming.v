(*

Some stuff here.

*)

(* * TYPES *)
Check 42.
Check true.
Check 42 + 2.
Check (42,true).
Check fun x : nat => x + 1.
Check fun x : nat => true.
(* END_TYPES *)

(* * BOOLEAN *)
Inductive boolean :=
| bTrue : boolean
| bFalse : boolean.

Check bTrue.
Check bFalse.

(* END_BOOLEAN *)


(* * BOOLEAN_FUNCTIONS *)

Definition bNot (b : boolean) : boolean :=

  match b with (* Pattern matching on b *)
  | bTrue => bFalse
  | bFalse => bTrue
  end.

Definition bAnd (a b : boolean) : boolean :=
  match a with
  | bTrue => b
  | _     => bFalse
  end.

Compute bNot bTrue.
Compute bNot bFalse.
Compute bAnd bFalse bFalse.
Check bAnd bFalse bFalse.

(* * END_BOOLEAN_FUNCTIONS *)


(* * NATS *)

Inductive Nat :=
| Zero : Nat           (* Zero is a natural number  *)
| Succ : Nat -> Nat.    (* n : Nat then Succ n : Nat *)

(* * END_NATS *)

(* * NAT_ADDITION *)

Fixpoint plus (a b : Nat) : Nat :=
  match a with
  | Zero     => b
  | Succ ap  => Succ (plus ap b)
  end.

Definition two := Succ (Succ Zero).

Compute plus two two.
(* * END_NAT_ADDITION *)

(* * NAT_THEOREMS *)

Theorem left_identity : forall n : Nat, plus Zero n = n.
Proof.
  trivial.
Qed.

Theorem right_identity : forall n : Nat, plus n Zero = n.
  (* Unlike in the previous case trivial will not work *)
  intro n.
  induction n.
  - trivial.  (* Base case *)
  - simpl. rewrite IHn. trivial. (* induction step *)
Qed.

(* END_NAT_THEOREMS *)
