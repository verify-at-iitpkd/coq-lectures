;; Org mode
(setq org-ditaa-jar-path (getenv "DITAA_JAR"))
(setq org-confirm-babel-evaluate nil) ; Prevent babel from asking for
				      ; confirmation.
(org-babel-do-load-languages
 'org-babel-load-languages
 '((ditaa . t)))
