(* This is a comment *)
(* This is a comment

  (* This is a nested comment *)

So you can use (* *) to fearlessly hide portions of the code from the
compiler.

  In C you cannot nest comment.


C-c C-n : Send the next "Line" to the coq interpreter and show the result
C-c C-b : This sends the entier buffer to the Coq interpretter
C-c C-RET : This will send all the "lines" Till the current point to the interpretter.

*)

Check 2.
Check true.
Check 2 + 3.
Compute 2 + 3.

(* Check 2 + true. *) (* This fails *)

(*

Every expression has a type

x : A   to mean x is of type A

*)

Check 2.
Check nat.
Check Set.
Check Type.

(* There is no artificial boundary between values and types *)

Check (2,true).


(* If A and B are two types then A * B is also a type.  And elements
   of A * B are precisely (a,b) where a : A, b : B)
*)

Check (2, nat).
Check (fun x => x + 1).
Compute ( (fun x => x + 1) 41).

Definition incr := fun x => x + 1.
Definition incrA (x : nat) := x + 1.


(*
Definition


*)

Compute incr 6 + 100.   (* This is equivalent to (incr 6) + 1 *)
Compute incr (6 + 100).

(* Exercise: Write a function double which will map x to 2 * x *)


(*
1. fun x => ....x .... x...    is that function that maps x to .... x ... x..

2. A -> B where A and B are types means the type of functions with domain A and range B.

Arrow is written as minus and greaterthan symbol

3. If f is a function and you want to apply it on a value v  f v

*)

Print incr.
Print incrA.
About incr.
About nat.
Print nat.

Definition crazyFn (b : bool) := if b then Type else bool.
Check crazyFn.

(*

Type is the "universe of all types"

*)

(* Print prints the definition of the symbol that you have given *)

(*
  x : A  it is the judgement that x is of type A
*)

(*   Types.

nat, bool etc are types (Sets)

A and B are types then A * B is a type (cartesian product of A and B)

A and B are types then A -> B is a type (type of functions from A to B)


 *)

(* There are only one argument functions.

*)
