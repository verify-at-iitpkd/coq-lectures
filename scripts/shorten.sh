#!/usr/bin/env bash

input=$1
output=$(echo $1 | sed s/.mp4/-shortened.mp4/)
end=$2
ffmpeg  -i "$input" -ss "00:00:00" -t "$end" "$output"
